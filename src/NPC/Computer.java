package ntu.csie.oop13spring;
import java.awt.*;
import javax.swing.*;

public class Computer extends Player{
    private Player other;
    private Behavior b = new Behavior(this);
    public Computer(Map m, POOPets p, Player _other){
        super(m,p,6,6);
        other = _other;
    }
    public Computer(Map m, POOPets p, Player _other, int d, int s, int div){
        super(m,p,6,6,d,s,div);
        other = _other;
    }
    private class Behavior extends AutoMove{
        int Tx = 8, Ty = 9;
        int ComDirectionX = 0, ComDirectionY = 0;
        int ComDirection = 0;
        int count = 0;
        Player _bind;

        public Behavior(MapObject mapObj){
            super(mapObj);
            _bind = (Player)bind;
        }

        public void Move(){
            count++;
            if(count < 10) return;
            count = 0;
            if(GUImain.startGame==0) return;
            if(checkArrive())
                generateTarget();
            reDirect();
            generateDirection();
            super.SetDirection(ComDirection);
            _bind.move(dx,dy,ComDirection);
        }

        public boolean checkArrive(){return (_bind.getMapX()==Tx && _bind.getMapY() == Ty);}

        public void generateTarget(){
            Tx = (int)(Math.random()*12 + 1);
            Ty = (int)(Math.random()*12 + 1);
        }

        public void reDirect(){
            ComDirectionX = (Tx - _bind.getMapX() < 0) ? LEFT : RIGHT;
            ComDirectionY = (Ty - _bind.getMapY() < 0) ? UP : DOWN;
        }

        public void generateDirection(){
            if(_bind.getMapX() == Tx){
                ComDirection = ComDirectionY;
                return;
            }else if(_bind.getMapY() == Ty){
                ComDirection = ComDirectionX;
                return;
            }
            if(((int)(Math.random()*100)) >= 50)
                ComDirection = ComDirectionX;
            else
                ComDirection = ComDirectionY;
        }
    }
};
