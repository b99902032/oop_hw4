package ntu.csie.oop13spring;
import java.awt.*;
import javax.swing.*;

public class Player extends MapObject{
    protected int x;
    protected int y;
    protected delayMove delay;
    protected POOPets self;
    
    private Player(Map m, POOPets p){
        this.self = p;
        delay = new delayMove(m,x,y);
    }
    public Player(Map m, POOPets p, int _x, int _y){
        this(m,p);
        x = _x;
        y = _y;
        m.requestMove(x,y,this);
    }
    public Player(Map m, POOPets p, int _x, int _y, int deTime, int s, int div){
        this.self = p;
        x = _x;
        y = _y;
        m.requestMove(x,y,this);
        delay = new delayMove(m,x,y,deTime,s,div);
    }
    public void move(int dx, int dy, int direction){
        if(delay.getStamp() > 0 ) return;
        if(self.getHP()==0){
            System.out.print("died!\n");
            System.exit(0);
        }
        int tx = changeLoc(x, dx, 15);
        int ty = changeLoc(y, dy, 14);
         self.changeImage(direction);
        if(delay.move(x, y, tx, ty, this)){
            x = tx;
            y = ty;
        }
    }

    private int changeLoc(int coord, int value, int max){
        if( coord + value < max)
            return (coord + value <= 0) ? 0 : coord + value;
        else
            return max;
    }

    public void draw(Graphics g){
        self.draw(g, getX(), getY());
    }

    public int getX(){
         return (delay.getStamp() > 0) ?  delay.getX() : x * 32;
    }
    public int getY(){
         return (delay.getStamp() > 0 ) ? delay.getY() : y * 32;
    }
    public int getHP(){
         return self.getHP();
    }
    public int getMP(){
        return self.getMP();
    }
    public int getAGI(){
        return self.getAGI();
    }
    public int getMapX(){ return x; }
    public int getMapY(){ return y; }
    public int offense(){ return 0; }
    public void BeAttacked(int ATK){self.BeAttacked(ATK);}
    public void act_1(){ self.act_1(x,y); }
    public void decMP(){ self.decMP();}
};
