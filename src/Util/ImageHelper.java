package ntu.csie.oop13spring;
import javax.swing.ImageIcon;
import javax.imageio.*;
import java.io.File;
import java.awt.*;
import java.awt.image.*;
import java.awt.Toolkit.*;


public class ImageHelper{
    public static Image readsubImage(String path, int x, int y, int w, int h){
        try{
            BufferedImage all = ImageIO.read(new File(path));
            BufferedImage pix = all.getSubimage(x*w, y*h, w, h);
            return (new ImageIcon(pix)).getImage();
        }catch(Exception e){
            return null;
        }
    }

}
