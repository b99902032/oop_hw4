package ntu.csie.oop13spring;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class delayMove implements ActionListener{
    private int Stamp = 0;
    private int oriX = 0, oriY = 0;
    private int dx = 0, dy = 0;
    private int CurX = 0, CurY = 0;
    private Timer timer;
    private Map map;
    private MapObject _obj;
    private int lastTimeStamp = 3;
    private int divide = 8;

    public delayMove(Map m){
        map = m;
        timer = new Timer(25, this);
        timer.start();
    }
    public delayMove(Map m, int x, int y){
        this(m);
        oriX = x;
        oriY = y;
    }
    public delayMove(Map m, int x, int y, int delayTime){
        map = m; oriX = x; oriY = y;
        timer = new Timer(delayTime, this);
    }
    public delayMove(Map m, int x, int y, int delayTime, int s, int div){
        this(m,x,y,delayTime);
        lastTimeStamp = s;
        divide = div;
    } 

    public boolean move(int x, int y, int tx, int ty, MapObject obj){
        if(!map.requestMove(tx,ty,obj))
            return false;
        Stamp = lastTimeStamp;
        oriX = x;
        oriY = y;
        dx = tx - x;
        dy = ty - y;
        CurX = oriX * 32 + dx * divide;
        CurY = oriY * 32 + dy * divide;
        _obj = obj;
        return true;
    }

    public void actionPerformed(ActionEvent e){
        Shiftmove();
    }

    private void Shiftmove(){
        if(Stamp == 0)
            return;
        else if(Stamp == 1)
            map.moveDone(oriX, oriY,_obj);
        Stamp = Stamp - 1;
        CurX = CurX + dx * divide;
        CurY = CurY + dy * divide;
    }

    public int getStamp(){ return Stamp; }
    public int getX(){ return CurX; }
    public int getY(){ return CurY; }
}
