package ntu.csie.oop13spring;
import java.awt.*;
import javax.swing.*;

public abstract class MapObject{
    public static final int LEFT  = 0;
    public static final int RIGHT = 1;
    public static final int DOWN  = 2;
    public static final int UP    = 3;
    public abstract void move(int x, int y, int direction);
    public abstract void draw(Graphics g);
    public abstract int offense();
    public abstract void BeAttacked(int attack);
    protected AutoMove auto;
}
