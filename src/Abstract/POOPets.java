package ntu.csie.oop13spring;
import java.awt.*;
import javax.swing.*;

public abstract class POOPets extends POOPet{
    public static final int LEFT  = 0;
    public static final int RIGHT = 1;
    public static final int DOWN  = 2;
    public static final int UP    = 3;

    public abstract void draw(Graphics g, int x, int y);
    public abstract void changeImage(int direction);
    public abstract void act_1(int x, int y);

    public void decMP(){
        int m = getMP();
        setMP(m - 1);
    }
    public void decHP(){
        int m = getHP();
        setHP(m - 1);
    }

    protected POOAction act(POOArena arena){
	return null;
    }
    protected POOCoordinate move(POOArena arena){
	return null;
    }
    public void BeAttacked(int ATK){
        int HP = getHP();
        setHP(HP - ATK);
        if(getHP()==0){
            System.out.print("died!\n");
            System.exit(1);
        }
    }
};
