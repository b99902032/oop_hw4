package ntu.csie.oop13spring;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

public abstract class AutoMove implements ActionListener{
    public static final int LEFT  = 0;
    public static final int RIGHT = 1;
    public static final int DOWN  = 2;
    public static final int UP    = 3;

    public Timer timer = new Timer(25, this);
    protected int dx = 0, dy = 0;
    protected int direction = 0;
    public MapObject bind; 

    public AutoMove(MapObject obj){
        this.bind = obj;
        timer.start();
    }
    public void actionPerformed(ActionEvent e){
        this.Move();    
    }
    public abstract void Move();

    public void SetDirection(int direction){
        switch(direction){
            case LEFT:
                dx = -1; dy = 0;
                break;
            case RIGHT:
                dx = 1; dy = 0;
                break;
            case DOWN:
                dx = 0; dy = 1;
                break;
            case UP:
                dx = 0; dy = -1;
                break;
        }
    }
}
