package ntu.csie.oop13spring;
import java.awt.*;
import javax.swing.*;

public abstract class FlyingObject extends MapObject{
    protected int currentX = 0, currentY = 0;
    protected Map map;
    private delayMove delay;
    private Behavior behavior = new FlyingObject.Behavior(this);
    public FlyingObject(Map m){
        delay = new delayMove(m);
        map = m;
    }
    public class Behavior extends AutoMove{
        public Behavior(MapObject mapobj){
            super(mapobj);
        }
        public void Move(){ bind.move(currentX,currentY,direction); }
    }

    public void setFly(int x, int y, int _direction){
        behavior.direction = _direction;
        behavior.SetDirection(_direction);
        currentX  = x;
        currentY  = y;
        //System.out.printf("%d %d\n",currentX,currentY);
        move(x, y, _direction);
    }
    
    public void move(int x, int y, int direction){
        if(delay.getStamp() > 0) return;
        if(!isActived()) return;
        int dx = behavior.dx;
        int dy = behavior.dy;
        if(delay.move(currentX, currentY, currentX+dx, currentY+dy, this)){
            currentX = currentX + dx;
            currentY = currentY + dy;
        }else
            Hit();
    }
    
    public void BeAttacked(int attack){}
    public void draw(Graphics g){  draw(g, currentX*32, currentY*32); }
    public abstract int offense();
    public abstract void Hit();
    public abstract boolean isActived();
    public abstract void draw(Graphics g, int x, int y);

}
