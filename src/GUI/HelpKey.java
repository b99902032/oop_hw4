package ntu.csie.oop13spring;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextArea;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;


public class HelpKey extends JFrame {


	/**
	 * 
	 */
	private static final long serialVersionUID = 263758201853066138L;
	private JPanel contentPane;

	/**
	 * Launch the application.
	 */

	/**
	 * Create the frame.
	 */
	public HelpKey() {
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
	        setBounds(250, 250, 382, 338);
                contentPane = new JPanel();
                contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
                setContentPane(contentPane);
                contentPane.setLayout(null);

                JTextArea txtrWUp = new JTextArea();
                txtrWUp.setText(" Up ----------------------- Arrow up");
                txtrWUp.setBounds(39, 69, 280, 37);
                contentPane.add(txtrWUp);

                JTextArea txtrADown = new JTextArea();
                txtrADown.setText(" Attack ------------------- z");
                txtrADown.setBounds(39, 40, 280, 37);
                contentPane.add(txtrADown);

                JTextArea txtrSLeft = new JTextArea();	
                txtrSLeft.setText(" Down --------------------- Arrow Down");
                txtrSLeft.setBounds(39, 100, 280, 37);
                contentPane.add(txtrSLeft);

                JTextArea txtrDright = new JTextArea();
                txtrDright.setText(" Left --------------------- Arrow Left");
                txtrDright.setBounds(39, 131, 280, 37);
                contentPane.add(txtrDright);

                JButton btnClose = new JButton("Close");
                btnClose.addActionListener(new ActionListener() {
                        public void actionPerformed(ActionEvent e) {
				setVisible();
                        }
                });
                btnClose.setBounds(141, 249, 87, 23);
                contentPane.add(btnClose);

                JTextArea txtrRightArrow = new JTextArea();
                txtrRightArrow.setText(" Right -------------------- Arrow Right");
                txtrRightArrow.setBounds(39, 165, 280, 31);
                contentPane.add(txtrRightArrow);

	}
        private static int visible = 0;
        public void setVisible(){
            if(visible==0){
                this.setVisible(false);
                visible = 1;
            }else{
                this.setVisible(true);
                visible = 0;
            }
        }
}
