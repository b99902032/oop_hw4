package ntu.csie.oop13spring;
import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.UIManager;
import javax.swing.JButton;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyAdapter;


public class GUImain extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7864610349160742112L;
	private JPanel contentPane;
	private HelpKey helpframe;
	public static int startGame = 0;
	/**
	 * Launch the application.
	 */
	public static void start() {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Throwable e) {
			e.printStackTrace();
		}
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUImain frame = new GUImain();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public GUImain() {
		setTitle("OOP HW4 Game");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(200, 0, 700, 650);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnFile = new JMenu("File");
		menuBar.add(mnFile);
		
		JMenuItem mntmReadMap = new JMenuItem("Read Map");
		mnFile.add(mntmReadMap);
		
		JMenuItem mntmExit = new JMenuItem("Exit");
		mnFile.add(mntmExit);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBounds(10, 510, 654, 33);
		contentPane.add(panel_1);
		panel_1.setLayout(null);
		
		final GameField mainGame = new GameField();
		final StatusBoard statBoard = new StatusBoard(mainGame.getPlayer(),mainGame.getComputer());
		contentPane.add(mainGame);
		mainGame.setLayout(null);		
		mainGame.add(statBoard);
		
		JButton btnStartGame = new JButton("Start Game");
		btnStartGame.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// start playing game
                                GUImain.startGame = 1;
                                mainGame.setFocusable(true);
                                mainGame.requestFocusInWindow();
				//mainGame.actionPerformed(e);
                                //statBoard.actionPerformed(e);
			}
		});
		btnStartGame.setBounds(180, 5, 100, 23);
		panel_1.add(btnStartGame);
		
		JButton btnHelp = new JButton("Help");
		btnHelp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// see help list
				if(helpframe==null){
					try {
						helpframe = new HelpKey();
						helpframe.setVisible(true);
					} catch (Exception e1) {
						e1.printStackTrace();
					}
				}else
                                    helpframe.setVisible();
                                mainGame.setFocusable(true);
                                mainGame.requestFocusInWindow();
			}
		});
		btnHelp.setBounds(430, 5, 81, 23);
		panel_1.add(btnHelp);
	}

}
