package ntu.csie.oop13spring;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class setInput{
    Controll controller = new Controll(); 
    Player player;
    public setInput(Player p){
        player = p;
    }
    public class Controll extends KeyAdapter{
        public void keyPressed(KeyEvent e){
            if(GUImain.startGame==0) return;
            int keycode = e.getKeyCode();
            switch(keycode){
                case KeyEvent.VK_LEFT:
                    player.move(-1,0,player.LEFT);
                    break;
                case KeyEvent.VK_RIGHT:
                    player.move(1,0,player.RIGHT);
                    break;
                case KeyEvent.VK_DOWN:
                    player.move(0,1,player.DOWN);
                    break;
                case KeyEvent.VK_UP:
                    player.move(0,-1,player.UP);
                    break;
                case KeyEvent.VK_Z:
                    player.act_1();
                    break;
            }
        }
        public void keyReleased(KeyEvent e){
        }
    }

}
