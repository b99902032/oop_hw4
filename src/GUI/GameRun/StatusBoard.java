package ntu.csie.oop13spring;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import javax.swing.ImageIcon;
import javax.imageio.*;
import java.io.File;
import java.awt.*;
import java.awt.image.*;
import java.awt.Toolkit.*;


public class StatusBoard extends JPanel implements ActionListener{
    private Timer timer;
    private Player player; 
    private Computer computer; 
    public StatusBoard(Player _player){
        this.timer = new Timer(25,this);
        this.setBounds(512, 0, 160, 480);
        this.setBackground(Color.GRAY);
        this.player = _player;
    }
    public StatusBoard(Player _player, Computer com){
        this(_player);
        computer = com;
    } 

    public void actionPerformed(ActionEvent e){
        repaint();
    }
    public void paint(Graphics g){
        super.paint(g);
	if(GUImain.startGame == 1){
            drawBackGround(g);
        }
    }
    private void drawBackGround(Graphics g){
         Image tmp = (new ImageIcon("./Image/statusPanel.png")).getImage();
         g.drawImage(tmp, 0, 0, null);
         drawAnimal(g);
    }
    private void drawAnimal(Graphics g){
        String path = "./Image/dora_blu.png";
        g.drawImage(readsubImage(path,0,0,32,48),64,64,null); 
        g.setColor(Color.YELLOW);
        g.setFont(new Font("Arial",Font.BOLD,16));
        g.drawString("Doraemon",40,40);
        g.drawString("HP: " + player.getHP(),40,144);
        g.drawString("MP: " + player.getMP(),40,176);
        path = "./Image/Pikachu.png";
        g.drawImage(readsubImage(path,0,1,32,32),64,280,null); 
        g.setColor(Color.RED);
        g.setFont(new Font("Arial",Font.BOLD,16));
        g.drawString("Enemy",40,256);
        g.drawString("HP: " + computer.getHP(),40,360);
        g.drawString("MP: " + computer.getMP(),40,392);
    }
    private Image readsubImage(String path, int x, int y, int w, int h){
        try{
            BufferedImage all = ImageIO.read(new File(path));
            BufferedImage pix = all.getSubimage(x*w, y*h, w, h);
            return (new ImageIcon(pix)).getImage();
        }catch(Exception e){
            return null;
        }
    }

}
