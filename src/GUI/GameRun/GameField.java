package ntu.csie.oop13spring;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import java.awt.image.*;
import java.awt.Toolkit.*;


public class GameField extends JPanel implements ActionListener{
    private Timer timer = new Timer(25,this);
    private Map map = new Map();
    private Player player = new Player(map, new POODoraemon(map), 3, 3);
    private Computer computer = new Computer(map, new POOPikachu(map), player);
    private setInput in = new setInput(player);

    public GameField(){
        timer.start();
        this.setBackground(Color.GRAY);
        this.setBounds(10, 10, 672, 480);
        this.addKeyListener(in.controller);
        this.setFocusable(true);
    }
    public void actionPerformed(ActionEvent e){
        repaint();
    }
    public void paint(Graphics g){
        super.paint(g);
        if(GUImain.startGame == 1){
	    map.draw(g);
            player.draw(g);
            computer.draw(g);
        }
    }
    public Player getPlayer(){ return player;}
    public Computer getComputer(){ return computer;}
}
