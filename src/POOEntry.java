package ntu.csie.oop13spring;
public class POOEntry extends POOArena{
    private int first_call = 0;
    public void show(){
        if(first_call == 0){
            GUImain.start();
            first_call++;
        }
    }

    private void pause(){
        while(true){try{System.in.read();}catch(Exception e){}}
    }

    public POOCoordinate getPosition(POOPet p){ return null;}
    public boolean fight(){ return true;}
}
