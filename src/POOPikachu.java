package ntu.csie.oop13spring;
import java.awt.*;
import javax.swing.*;

class POOPikachu extends POOPets{
    private static final int sizeX = 32;
    private static final int sizeY = 32;
    private static final int CutY = 0;
    private static boolean first_call = false;
    private static Image DOWN_I, LEFT_I, RIGHT_I, UP_I;
    private static Knife knife;
    private Image image;
    public POOPikachu(Map m){
        if(first_call == false){
            String path = "./Image/Pikachu.png";
            DOWN_I  = ImageHelper.readsubImage(path, 0, 1, sizeX, sizeY);
            LEFT_I  = ImageHelper.readsubImage(path, 0, 2, sizeX, sizeY);
            RIGHT_I = ImageHelper.readsubImage(path, 0, 3, sizeX, sizeY);
            UP_I    = ImageHelper.readsubImage(path, 0, 0, sizeX, sizeY);
            first_call = true;
            knife = new Knife(m,this);
        }
        setHP(8);
        setMP(20);
        image = UP_I;
    }
    public void draw(Graphics g, int x, int y){
        g.drawImage(image, x, (y-CutY), null);
        knife.draw(g);
    }
    public void changeImage(int direction){
        switch(direction){
            case LEFT:
                image = LEFT_I;
                break;
            case RIGHT:
                image = RIGHT_I;
                break;
            case UP:
                image = UP_I;
                break;
            case DOWN:
                image = DOWN_I;
                break;
        } 
    }
    public void act_1(int x, int y){
        int direction = 0;
        if(image == LEFT_I){
            direction = LEFT;
        }else if(image == RIGHT_I){
            direction = RIGHT;
        }else if(image == DOWN_I){
            direction = DOWN;
	}else if(image == UP_I){
	    direction = UP;
	}
        knife.setFly(x,y,direction);
    }
};
