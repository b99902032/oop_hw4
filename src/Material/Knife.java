package ntu.csie.oop13spring;
import java.awt.*;
import javax.swing.*;

class Knife extends FlyingObject{
    private static int sizeX = 32, sizeY = 32;
    private static Image LEFT_I, RIGHT_I, UP_I, DOWN_I;
    private Image image;
    private static boolean first_call = false;
    private POOPets pet;
    public Knife(Map m, POOPets p){
        super(m);
        if(first_call==false){
            String path = "./Image/123.png";
            DOWN_I  = ImageHelper.readsubImage(path, 0, 0, sizeX, sizeY);
            LEFT_I  = ImageHelper.readsubImage(path, 1, 0, sizeX, sizeY);
            RIGHT_I = ImageHelper.readsubImage(path, 1, 1, sizeX, sizeY);
            UP_I    = ImageHelper.readsubImage(path, 0, 1, sizeX, sizeY);
            first_call = true;
        }
        image = null;
        pet = p;
    }
    public void setFly(int x, int y, int direction){
        if(image!=null) return;
        if(pet.getMP()==0)
            pet.decHP();
        else
            pet.decMP();
        System.out.print("shoot!\n");
        changeImage(direction);
        super.setFly(x,y,direction);
    }
    public void draw(Graphics g, int x, int y){
        if(image!=null)
            g.drawImage(image, x, y, null);
    }
    public int offense(){ return 1; }
    public boolean isActived(){ return (image!=null);} 
    public void Hit(){
        if(image!=null){
            changeImage(-1);
            map.moveDone(currentX,currentY,this);
        }
    }

    public void changeImage(int direction){
        switch(direction){
            case LEFT:
                image = LEFT_I;
                break;
            case RIGHT:
                image = RIGHT_I;
                break;
            case UP:
                image = UP_I;
                break;
            case DOWN:
                image = DOWN_I;
                break;
            case -1:
                image = null;
                break;
        } 
    }
};
