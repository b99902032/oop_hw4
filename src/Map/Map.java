package ntu.csie.oop13spring;
import javax.swing.ImageIcon;
import javax.imageio.*;
import java.util.*;
import java.io.File;
import java.awt.*;

public class Map{
    
    private String _Map[] = new String[16];
    private String _MapB[] = new String[16];
    private boolean _Moveable[][] = new boolean[16][16];
    private MapObject _lock[][] = new MapObject[16][16];
    private mapHelper _Helper = new mapHelper();
    private Terrain _Terrain = new Terrain();

    public Map(){
        _Map  = _Helper.readMap("./Maps/Map.txt");
        _MapB = _Helper.readMap("./Maps/MapB.txt");
        for(int row = 0; row < 16; row++)
            for(int col = 0; col < 16; col++){
                _Moveable[row][col] = (_MapB[col].charAt(row)=='0')?true:false;
                _lock[row][col] = null;
            }
    }

    public void draw(Graphics g){
        for(int row=0;row<15;row++)
            for(int col=0;col<16;col++){
                char obstacle = _MapB[row].charAt(col);
                char background = _Map[row].charAt(col);
                g.drawImage(_Terrain.getIdx(background), col*32, row*32, null);
                g.drawImage(_Terrain.getIm(obstacle),  col*32, row*32, null);
            }
    }
    public String[] getObstacle(){ return _MapB;}

    public boolean requestMove(int row, int col, MapObject obj){
        if(_Moveable[row][col]){
            _Moveable[row][col] = false;
            _lock[row][col] = obj;
            return true;
        }else if(_lock[row][col]!=null){
            System.out.print("attack!!\n\n");
            _lock[row][col].BeAttacked(1);
        }
        return false;
    }

    public void moveDone(int row, int col, MapObject obj){
        if(!_Moveable[row][col] && _lock[row][col]==obj){
            _Moveable[row][col] = true;
            _lock[row][col] = null;
            return;
        }
        if(_Moveable[row][col])
            System.out.printf("Something wrong!! %d %d\n",row,col);
    }

    private class mapHelper{
        private String[] readMap(String path){
            String _tmp[] = new String[16];
            try{
                Scanner m = new Scanner(new File(path));
                for(int i=0;m.hasNext();i++)
                    _tmp[i] = m.nextLine();
                m.close();
            }catch(Exception e){
                System.out.println("error loading map");
            }
            return _tmp;
        }
    }

}
