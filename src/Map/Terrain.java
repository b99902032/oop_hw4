package ntu.csie.oop13spring;
import java.awt.*;
import javax.swing.*;
public class Terrain{
    private Image grass;
    private Image wall;
    
    public Terrain(){
        grass = (new ImageIcon("./Image/grass.png")).getImage();
        wall = ImageHelper.readsubImage("./Image/3.png",3,3,32,32);
    }
    private Image choose(char c){
        switch(c){
            case 'g':
                return grass;
            case 'w':
                return wall;
            default:
                return null;
        }
    }
    public Image getIdx(char c){
        return this.choose(c);
    }
    public Image getIm(char c){
        return (c == '0') ? grass : wall;
    }
}

